﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour {

	public static MainMenuManager instance;

    public RectTransform selectScreen;
    public RectTransform creditScreen;
    public RectTransform optionsScreen;
    public RectTransform continueScreen;
    public RectTransform newGameScreen;

    public Selectable continueGameSelectable;
    public Selectable newGameSelectable;
    public Selectable optionsSelectable;
    public Selectable creditScreenSelectable;
    public Selectable back;

    public enum Screen {
        Select,
        Credit,
        Options,
        NewGame,
        Continue,
        Back
    }

    public Screen currentScreen = Screen.Select;

    void Awake () {
        if (instance == null) instance = this;
        ShowSelectScreen();
        deleteFilePrompt.SetActive(false);
    }

    void Update () {
        if (Input.GetKeyDown("space")) {
            switch (currentScreen) {
                case Screen.Select:
                    ShowSelectScreen();
                    break;
                case Screen.Credit:
                    ShowCreditScreen();
                    break;
                case Screen.Options:
                    ShowOptionsScreen();
                    break;
                case Screen.Continue:
                    ShowContinueScreen();
                    break;
                case Screen.NewGame:
                    ShowNewGameScreen();
                    break;
                case Screen.Back:
                    GoBack();
                    break;
                default:
                    break;
            }
        }
    }

    public void ShowSelectScreen () {
        currentScreen = Screen.Select;
        EventSystem.current.SetSelectedGameObject(null);
		selectScreen.gameObject.SetActive(true);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        continueScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(false);
    }

    public void ShowCreditScreen () {
        currentScreen = Screen.Credit;
		EventSystem.current.SetSelectedGameObject(null);
		selectScreen.gameObject.SetActive(false);
		creditScreen.gameObject.SetActive(true);
        optionsScreen.gameObject.SetActive(false);
        continueScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(false);
	}

    public void ShowOptionsScreen () {
        currentScreen = Screen.Options;
        EventSystem.current.SetSelectedGameObject(null);
		selectScreen.gameObject.SetActive(false);
		creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(true);
        continueScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(false);
    }

    public void GoBack () {
        currentScreen = Screen.Select;
        EventSystem.current.SetSelectedGameObject(null);
		selectScreen.gameObject.SetActive(true);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        continueScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(false);
    }

    public void ShowContinueScreen () {
        currentScreen = Screen.Continue;
        EventSystem.current.SetSelectedGameObject(null);
		selectScreen.gameObject.SetActive(false);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        continueScreen.gameObject.SetActive(true);
        newGameScreen.gameObject.SetActive(false);
    }

    public void QuitGame () {
        Application.Quit();
    }

    public void ShowNewGameScreen () {
        currentScreen = Screen.NewGame;
        EventSystem.current.SetSelectedGameObject(null);
		selectScreen.gameObject.SetActive(false);
        creditScreen.gameObject.SetActive(false);
        optionsScreen.gameObject.SetActive(false);
        continueScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(true);
    }

    private FileSelectController fileSelect;
    public GameObject deleteFilePrompt;
    public void PromptDelete (FileSelectController fileSelect) {
        this.fileSelect = fileSelect;
        deleteFilePrompt.SetActive(false);
    }

    public void ConfirmDelete (bool confirm) {
        deleteFilePrompt.SetActive(false);
        if (confirm) {
            fileSelect.ConfirmDelete();
        }
    }
}

