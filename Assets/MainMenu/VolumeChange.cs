﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeChange : MonoBehaviour {

    private AudioSource audioSource;
    private float musicVolume = 10f;


	void Start () {
		audioSource = GetComponent<AudioSource>();
	}
	
	void Update () {
        audioSource.volume = musicVolume;
	}

    public void SetVolume(float vol) {
        musicVolume = vol;
    }
}

