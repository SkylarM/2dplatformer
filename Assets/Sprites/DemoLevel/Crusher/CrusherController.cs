﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrusherController : MonoBehaviour {

	Animator anim;
	void Awake () {
		anim = GetComponent<Animator>();
	}

    private void Start()
    {
        StartCoroutine(Crusher());
    }

    IEnumerator Crusher ()
    {
        yield return new WaitForEndOfFrame();
		while (enabled) {
			anim.SetTrigger("GoDown");
            //AudioManager.instance.PlaySFX("CrusherSE"); Commented out due to sound effect being global -Josh
			yield return new WaitForSeconds(1);
			anim.SetTrigger("GoUp");
			yield return new WaitForSeconds(1);
		}
        yield return new WaitForEndOfFrame();
	}
}
