﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletHit : MonoBehaviour {

    public float damage = 1;

    void OnCollisionEnter2D(Collision2D c) {
        HitObject(c.gameObject);
    }

    void OnTriggerEnter2D(Collider2D c) {
        HitObject(c.gameObject);
    }

    void HitObject(GameObject g) {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null && !g.CompareTag("Robot")) {
            health.TakeDamage(damage);
        }
    }
}
