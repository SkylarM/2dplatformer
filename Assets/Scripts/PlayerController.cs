﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMotor), typeof(HealthController))]
public class PlayerController : MonoBehaviour {
	
	public static PlayerController instance;
	public GameObject deathScreen;
	public float jumpWindow = 0.05f;
    public bool isStopped;
	bool isJumping = false;
	public bool sliding = false;
	float slideTimer = 0f;
	public float maxSlideTime = 1.5f;
    public bool canClimb;

    private float lastJumpPress = -1;
    public static bool canMove = true;
    private bool attacking = false;

	HealthController health;

	[SerializeField]
	GameObject playerCollider;

	CharacterMotor motor;
	Animator anim;

	Rigidbody2D rb;

	void Awake ()
    {
		if (instance == null)
        {
			DontDestroyOnLoad(gameObject);
			instance = this;
			isStopped = false;
			canClimb = false;
			deathScreen.SetActive(false);
			health = GetComponent<HealthController>();
		}
		else
        {
			instance.transform.position = transform.position;
			Destroy(gameObject);
		}
    }

	void Start ()
    {
        motor = GetComponent<CharacterMotor>();
		anim = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();
		//Cursor.visible = false;
	}

	void OnEnable ()
    {
		health.onHealthChanged += HealthChanged;
	}

	void OnDisable ()
    {
		if (health) health.onHealthChanged -= HealthChanged;
	}

	void Update () {
		if (health.health <= 0)
        {
			return;
		}

        float x = Input.GetAxisRaw("Horizontal") * motor.speed;
        anim.SetFloat("speed", motor.body.velocity.magnitude);

        
        motor.shouldJump = (Time.time - lastJumpPress) < jumpWindow; // jump even if you hit the button slightly before you've landed
        motor.cancelJump = !Input.GetButton("Jump");
        motor.targetVelocity = new Vector2(x, motor.body.velocity.y);
        
		CameraController cam = CameraController.instance;

		if (cam)
        {
				cam.offset.x = transform.right.x * 3;
				cam.offset.y = 0.5f * 3;
		}

		if (isStopped)
        {
			motor.targetVelocity = Vector3.zero;
			CameraController.instance.camMove = true;
			StartCoroutine(CameraMove());
		}

		if (Input.GetButtonDown("Jump"))
        {
			isJumping = true;
			anim.SetTrigger("jumping");
            //audio.PlayOneShot(jumpSE);
            lastJumpPress = Time.time;
		}

        if (canClimb)
        {
            float y = Input.GetAxisRaw("Vertical") * motor.speed;
            motor.targetVelocity = new Vector2(x, y);
        }

        if (Input.GetButtonDown("Fire1") && !attacking) {
			anim.SetTrigger("Attack1");
            AudioManager.instance.PlaySFX("HittingWithSwordSE");
            attacking = true;
		}

		if (Input.GetButtonDown("Fire3") && motor.onGround && !sliding) {
			slideTimer = 0f;
			anim.SetBool("isSliding", true);
			//audio.PlayOneShot(slidingSE);
			gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
			sliding = true;
		}

		if (sliding)
        {
			slideTimer += Time.deltaTime;
			if (slideTimer > maxSlideTime)
            {
				sliding = false;
				anim.SetBool("isSliding", false);
				gameObject.GetComponent<CapsuleCollider2D>().enabled = true;
			}
		}
        else if (motor.isWallSliding)
        {
            anim.SetBool("wallSlide", true);
        }
		else {
			anim.SetBool("wallSlide", false);
		}
	}

	void HealthChanged (float previousHealth, float health)
    {
		if (previousHealth > health)
        {
			if (health <= 0)
            {
                motor.targetVelocity = Vector2.zero;
                rb.velocity = Vector2.zero;
				anim.SetTrigger("death");
				deathScreen.SetActive(true);
			}
			else if (previousHealth > health)
            {
				anim.SetTrigger("takeHit");
			}
		}
	}

	void OnTriggerEnter2D (Collider2D c)
    {
        
		if (c.gameObject.CompareTag("CameraAnim"))
        {
			isStopped = true;
		}
		if (c.gameObject.CompareTag("HealthRegen"))
        {
			anim.SetBool("resting", true);
		}
		if (c.gameObject.CompareTag("DeathTrigger"))
        {
			CameraShake.instance.shakeDuration = 0.5f;
		}
	}

	void OnTriggerExit2D (Collider2D c)
    {
		if (c.gameObject.CompareTag("HealthRegen"))
        {
			anim.SetBool("resting", false);
		}
	}

	IEnumerator CameraMove ()
    {
		yield return new WaitForSeconds(5);
		isStopped = false;
	}

    public void AttackOver()
    {
        attacking = false;
    }
}