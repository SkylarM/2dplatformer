﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathTriggersWater : MonoBehaviour {

    private GameObject deathScreen;

    public Transform respawnSpot;
	public float damage = 5;

    void OnTriggerEnter2D(Collider2D c) {
		if (c.gameObject.CompareTag("Player")) {
			c.gameObject.transform.position = respawnSpot.transform.position;
       		HitObject(c.gameObject);
		}
    }

    void Awake()
    {
        deathScreen = GameObject.Find("DeathScreen");
    }

    void HitObject(GameObject g) {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null) {
            health.TakeDamage(damage);
        }
    }
}

