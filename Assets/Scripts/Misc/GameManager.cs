﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public GameObject loadingScreen;

	public static GameManager instance;

	private GameObject levelSelect;
	public Slider loadingBar;
	
	void Awake () {
		if (instance == null) {
			DontDestroyOnLoad(gameObject);
			instance = this;
			loadingScreen.SetActive(false);
		}
		else {
			Destroy(gameObject);
		}
	}

	public static void LoadScene (int num) {
		instance.StartCoroutine(instance.Loading(num));
	}

	IEnumerator Loading (int num) {
		loadingScreen.SetActive(true);
		AsyncOperation a = SceneManager.LoadSceneAsync(num);

		while (!a.isDone) {
			loadingBar.value = a.progress;
			yield return new WaitForEndOfFrame();
		}

		loadingScreen.SetActive(false);
	}
}
