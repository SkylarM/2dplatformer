﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleporterScript : MonoBehaviour {

	public bool restAreaPortal;
	public bool isPortal1;

    public bool isStopped;

    private void Start()
    {
        StartCoroutine (PortalSE());
    }

    void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.CompareTag("Player") && restAreaPortal) {
			LevelSelectSingleton.instance.gameObject.SetActive(true);
		}

		if (c.gameObject.CompareTag("Player") &&isPortal1) {
			SceneManager.LoadScene(1);
		}
    }

	void OnTriggerExit2D (Collider2D c) {
		if (c.gameObject.CompareTag("Player") &&restAreaPortal) {
			LevelSelectSingleton.instance.gameObject.SetActive(false);
		}
	}

	public void LoadLevel (int num) {
		LevelSelectSingleton.instance.gameObject.SetActive(false);
		GameManager.LoadScene(num);
	}

    IEnumerator PortalSE()
    {
        yield return new WaitForSeconds(0.1f);
        AudioManager.instance.PlaySFX("PortalSE");
    }

}
