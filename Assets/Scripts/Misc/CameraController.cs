﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour {

	public static CameraController instance;

    public bool camMove;
	public Vector3 offset;
	public Transform target;
	public float speed = 1;
	public float minDistToMove = 1;


	void Awake ()
    {
		if (instance == null)
        {
			instance = this;
		}

        else
        {
            Destroy(gameObject);
        }
	}

	void Update () {
		if (target == null) target = GameObject.FindGameObjectWithTag("Player").transform;
		if (Application.isPlaying) {
			float d = Vector2.Distance(target.position + offset, transform.position);
			if (d > minDistToMove) {
				transform.position = Vector3.Lerp(transform.position, target.position + offset, Time.deltaTime * speed);
			}
		}
		else {
			transform.position = target.position + offset;
		}

    }
}
