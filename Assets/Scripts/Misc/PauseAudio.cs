﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseAudio : MonoBehaviour {

    private float lastScale;
    private bool paused = false;

    private void Start()
    {
        lastScale = Time.timeScale;
    }

    void Update () {
        if (lastScale != Time.timeScale && paused == false)
        {
            lastScale = Time.timeScale;
            GetComponent<AudioSource>().volume = 0;
            AudioManager.instance.PlaySFX("PauseTheGameSE");
            paused = true;
        }
        else if(lastScale != Time.timeScale && paused == true)
        {
            lastScale = Time.timeScale;
            GetComponent<AudioSource>().volume = 1;
            AudioManager.instance.PlaySFX("ResumeTheGameSE");
            paused = false;
        }
    }
}
