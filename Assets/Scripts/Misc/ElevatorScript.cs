﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorScript : MonoBehaviour {

	bool isTouching;

	Animator anim;

	void Awake () {
		anim = GetComponent<Animator>();
	}

	void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.CompareTag("Player")) {
			anim.SetTrigger("goUp");
		}
	}

	void OnTriggerExit2D (Collider2D c) {
		if (c.gameObject.CompareTag("Player")) {
			anim.SetTrigger("goDown");
		}
	}

}
