﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectSingleton : MonoBehaviour {

	public static LevelSelectSingleton instance;

	void Awake () {
		if(instance == null)
		{
			instance = this;
		}
		gameObject.SetActive(false);
	}
}
