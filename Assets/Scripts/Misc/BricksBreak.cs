﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BricksBreak : MonoBehaviour {

	Rigidbody body;
	Animator anim;
	public GameObject breakable;

	HealthController health;


	void Awake () {
		body = GetComponent<Rigidbody>();
		anim = GetComponent<Animator>();
		health = GetComponent<HealthController>();
	}

	void OnEnable () {
		health.onHealthChanged += HealthChanged;
	}

	void OnDisable () {
		health.onHealthChanged -= HealthChanged;
	}

	void HealthChanged (float previousHealth, float health) {
		if (previousHealth > health) {
			if (previousHealth > 0 && health == 0) {
				AudioManager.instance.PlaySFX("BlockSmashSE");
				breakable.SetActive(false);
			}

			else if (previousHealth > health) {
				//Damage happens here
			}
		}
	}
}
