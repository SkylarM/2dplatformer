﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour {

	public static PauseMenuManager instance;

	public RectTransform pauseMenuRoot;
    public AudioClip pauseTheGameSE;
    public AudioClip resumeTheGameSE;
	public GameObject levelSelect;
	public GameObject deathScreen;

	public float healHealth = 1;

	void Awake () {
		if (instance == null) {
			DontDestroyOnLoad(gameObject);
			instance = this;
	}
		else {
			Destroy(gameObject);
		}
		deathScreen.SetActive(false);
		ResumeGame();
	}

	public void PauseGame () {
		pauseMenuRoot.gameObject.SetActive(true);
		Time.timeScale = 0;
	}

	public void ResumeGame () {
		pauseMenuRoot.gameObject.SetActive(false);
		Time.timeScale = 1;
	}

	public void QuitGame () {
		Application.Quit();
	}

	public void SaveGame () {
		SaveManager.Save();
	}

	void Update () {
		if (Input.GetKeyDown("escape")) {
            //PlayerController.instance.audio.PlayOneShot(pauseTheGameSE);
            if (pauseMenuRoot.gameObject.activeSelf) ResumeGame();
			else PauseGame();
		}
	}

	public void Restart () {
		deathScreen.SetActive(false);
		SceneManager.LoadScene(1);
		PlayerController.instance.GetComponent<HealthController>().health = healHealth;
		PlayerController.instance.GetComponent<Animator>().SetTrigger("restart");
	}

	public void Returntomenu () {
		deathScreen.SetActive(false);
    	SceneManager.LoadScene(0);
		PlayerController.instance.GetComponent<HealthController>().health = healHealth;
		PlayerController.instance.GetComponent<Animator>().SetTrigger("restart");
}

}

