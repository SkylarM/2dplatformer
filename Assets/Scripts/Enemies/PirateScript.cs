﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PirateScript : MonoBehaviour {

    private HealthController health;
    private Animator anim;
    private GameObject player;
    private bool dead = false;

    public GameObject arms;
    public GameObject gun;

    private void Awake()
    {
        
    }

    private void Start()
    {
        player = PlayerController.instance.gameObject;
        health = GetComponent<HealthController>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(health.health == 0 && !dead)
        {
            anim.SetTrigger("Dead");
            dead = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = !gameObject.GetComponent<BoxCollider2D>().enabled;
        }
    }

    public void Alert()
    {
        if (!dead)
        {
            Quaternion rotation = Quaternion.LookRotation(player.transform.position - transform.position, transform.TransformDirection(Vector3.up));
            arms.transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
            anim.SetTrigger("Fire");
        }
    }

    public void Fire()
    {
        BulletSpawnerPirate.instance.SpawnBullet(gun.transform.position, gun.transform);
    }
}
