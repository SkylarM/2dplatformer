﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHitPirate : MonoBehaviour {

    public float damage = 1;
    Rigidbody2D rb;

    private void OnEnable()
    {
        StartCoroutine(Expire());
    }


    void OnTriggerEnter2D (Collider2D c)
    {
        HitObject(c.gameObject);
        if (c.CompareTag("Pirate") || (c.gameObject.layer == LayerMask.NameToLayer("Environment")))
        {
            gameObject.SetActive(false);
        }
        
    }

    void HitObject (GameObject g)
    {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null && !g.CompareTag("Pirate"))
        {
            health.TakeDamage(damage);
        }
    }

    IEnumerator Expire()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
    }
}
