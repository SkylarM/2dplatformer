﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RobotController : MonoBehaviour
{
    private HealthController health;
    private CharacterMotorEnemies motor;
    private Rigidbody2D rb;
    private Animator anim;
    private GameObject g;
    private Rigidbody2D lazerRB;
    private BoxCollider2D robot;
    private CircleCollider2D headCollider;
    public AudioClip laserShotSE;

    public LayerMask layerMask;
    public Transform gun1;
    public Transform gun2;
    public GameObject lazer;
    public GameObject head;

    public Slider healthSlider;

    int previousState;
    bool dead;
    bool attacking;

    public enum State
    {
        Idle,
        Advance,
        FlailAttack,
        Attack,
        Death
    }

    public State state = State.Idle;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        motor = GetComponent<CharacterMotorEnemies>();
        health = GetComponent<HealthController>();
        robot = GetComponent<BoxCollider2D>();
        headCollider = GetComponentInChildren<CircleCollider2D>();
    }

    void Update()
    {
        if (health.health <= 0)
        {
            state = State.Death;
        }
        switch (state)
        {
            case State.Idle:
                IdleUpdate();
                break;
            case State.Advance:
                AdvanceUpdate();
                break;
            case State.FlailAttack:
                StartCoroutine(FlailAttackUpdate());
                break;
            case State.Attack:
                AttackUpdate();
                break;
            case State.Death:
                DeathUpdate();
                break;
            default:
                Debug.Log("Default");
                break;
        }
    }

    void IdleUpdate()
    {
        motor.targetVelocity = motor.targetVelocity * 0;
        anim.SetBool("advance", false);
        if (Vector2.Distance(PlayerController.instance.transform.position, this.transform.position) < 12)
        {
            state = State.Advance;
        }
    }

    void AdvanceUpdate()
    {
        anim.SetBool("advance", true);
        motor.targetVelocity = (PlayerController.instance.transform.position - transform.position).normalized;
        if (Physics2D.Raycast(gun1.position, -gun1.right, 40, layerMask))
        {
            state = State.Attack;
        }
        if (Vector2.Distance(PlayerController.instance.transform.position, this.transform.position) < 2)
        {
            anim.SetTrigger("attackOver");
            state = State.FlailAttack;
        }
    }

    IEnumerator FlailAttackUpdate()
    {
        yield return new WaitForSeconds(.1f);
        anim.SetTrigger("armFlail");
        motor.targetVelocity = (PlayerController.instance.transform.position - transform.position).normalized;
        yield return new WaitForSeconds(8);
        anim.SetTrigger("attackOver");
        state = State.Idle;
    }

    void AttackUpdate()
    {
        if(!attacking)
        {
            anim.SetBool("advance", false);
            motor.targetVelocity = motor.targetVelocity * 0;
           // PlayerController.instance.audio.PlayOneShot(laserShotSE);
            anim.SetTrigger("fire");
            attacking = true;
            StartCoroutine(FireTimer());
        }
    }

    void DeathUpdate()
    {
        if (!dead)
        {
            anim.SetBool("advance", false);
            anim.SetTrigger("dead");
            dead = true;
            motor.targetVelocity = motor.targetVelocity * 0;
        }
        motor.targetVelocity = motor.targetVelocity * 0;
        
    }

    IEnumerator FireTimer()
    {
        yield return new WaitForSeconds(1.5f);
        BulletSpawner.instance.SpawnBullet(gun1.position);
        BulletSpawner.instance.SpawnBullet(gun2.position);
        yield return new WaitForSeconds(.2f);
        BulletSpawner.instance.SpawnBullet(gun1.position);
        BulletSpawner.instance.SpawnBullet(gun2.position);
        yield return new WaitForSeconds(.2f);
        BulletSpawner.instance.SpawnBullet(gun1.position);
        BulletSpawner.instance.SpawnBullet(gun2.position);
        yield return new WaitForSeconds(1);
        state = State.Idle;
        attacking = false;
    }

    void HeadRoll()
    {
        head.transform.SetParent(null);
        head.SetActive(true);
        robot.enabled = !robot.enabled;
    }
}
