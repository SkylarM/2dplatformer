﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour {

    public static BulletSpawner instance;

    Rigidbody2D rb;

    public GameObject bulletPrefab;
    public int bulletPoolSize = 5;
    GameObject[] bulletPool;
    int bulletIndex;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        bulletPool = new GameObject[bulletPoolSize];
        for (int i = 0; i < bulletPoolSize; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.transform.parent = this.gameObject.transform;
            bullet.SetActive(false);
            bulletPool[i] = bullet;
        }
    }

    public void SpawnBullet(Vector3 pos)
    {
        GameObject bullet = bulletPool[bulletIndex];
        bullet.transform.position = pos;
        bullet.SetActive(true);
        rb = bullet.GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.left*20;
        bulletIndex++;
        bulletIndex %= bulletPoolSize;
    }
}
