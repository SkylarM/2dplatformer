﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TikiAttack : MonoBehaviour {

    public delegate void OnTriggerEvent();
    public static event OnTriggerEvent EnterTrigger = delegate {};
    public static event OnTriggerEvent ExitTrigger = delegate {};
    public static TikiAttack instance;

    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.CompareTag("Player")) EnterTrigger();
    }

    private void OnTriggerExit2D(Collider2D c)
    {
        if (c.CompareTag("Player")) ExitTrigger();
    }
}
