﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHit : MonoBehaviour {

    public float damage = 1;
    Rigidbody2D rb;

    public bool isBullet;

    void OnCollisionEnter2D (Collision2D c) {
        HitObject(c.gameObject);
    }

    void OnTriggerEnter2D (Collider2D c) {
        if (c.gameObject.CompareTag("Player") && isBullet) {
            gameObject.SetActive(false);
        }
        HitObject(c.gameObject);
    }

    void HitObject (GameObject g) {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null && !g.CompareTag("Robot")) {
            health.TakeDamage(damage);
        }
    }
}
