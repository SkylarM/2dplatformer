﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawnerPirate : MonoBehaviour {

    public static BulletSpawnerPirate instance;

    Rigidbody2D rb;

    public int bulletSpeed = 200;
    public GameObject bulletPrefab;
    public int bulletPoolSize = 5;
    GameObject[] bulletPool;
    int bulletIndex;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        
        bulletPool = new GameObject[bulletPoolSize];
        for (int i = 0; i < bulletPoolSize; i++)
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.SetActive(false);
            bullet.transform.parent = this.gameObject.transform;
            bulletPool[i] = bullet;
        }


    }

    public void SpawnBullet(Vector3 pos, Transform trans)
    {
        GameObject bullet = bulletPool[bulletIndex];
        bullet.transform.position = pos;
        bullet.SetActive(true);
        bullet.transform.rotation = trans.rotation;
        rb = bullet.GetComponent<Rigidbody2D>();
        rb.velocity = -trans.right * bulletSpeed;
        bulletIndex++;
        bulletIndex %= bulletPoolSize;
    }
}
