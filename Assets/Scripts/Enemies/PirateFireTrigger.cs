﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PirateFireTrigger : MonoBehaviour
{

    PirateScript pirate;

    void Start()
    {
        pirate = GetComponentInParent<PirateScript>();
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && other.GetComponent<HealthController>().health > 0)
        {
            pirate.Alert();
        }
    }

}
