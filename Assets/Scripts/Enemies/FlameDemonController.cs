﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameDemonController : MonoBehaviour {

    public static FlameDemonController instance;

    public GameObject player;
    public Animator lavaAnim;

    public static bool rightSide;
    public static bool leftSide;

    private Animator anim;
    private int vulnerableCheck;
    private int attackType;
    private bool attacking;

    void Start ()
    {
        if (instance == null) instance = this;
        player = PlayerController.instance.GetComponent<GameObject>();
        anim = GetComponent<Animator>();
        vulnerableCheck = 0;
	}
	
	void Update ()
    {
        if (attacking) return;

        if (vulnerableCheck == 1)
        {
            StartCoroutine(Vulnerable());
        }
        else if (rightSide)
        {
            StartCoroutine(AttackRight());
        }
        else if (leftSide)
        {
            StartCoroutine(AttackLeft());
        }
        else
        {
            attackType = Random.Range(1,3);
            if (attackType == 1)
            {
                attackType = 0;
                ArmRise();
            }
            else if (attackType == 2)
            {
                attacking = true;
                attackType = 0;
                anim.SetTrigger("swim");
            }
        }        
    }

    void ArmRise()
    {
        attacking = true;
        anim.SetTrigger("surge");
    }

    IEnumerator LavaRise()
    {
        lavaAnim.SetTrigger("rise");
        yield return new WaitForSeconds(7.5f);
        anim.SetTrigger("swim");
        vulnerableCheck = Random.Range(1, 6);
    }

    IEnumerator AttackLeft()
    {
        attacking = true;
        anim.SetTrigger("surgeLeft");
        yield return new WaitForSeconds(2);
        vulnerableCheck = Random.Range(1, 6);
        attacking = false;
    }

    IEnumerator AttackRight()
    {
        attacking = true;
        anim.SetTrigger("surgeRight");
        yield return new WaitForSeconds(2);
        vulnerableCheck = Random.Range(1, 6);
        attacking = false;
    }

    IEnumerator Vulnerable()
    {
        attacking = true;
        vulnerableCheck = 0;
        anim.SetTrigger("vulnerable");
        yield return new WaitForSeconds(1);
        attacking = false;
    }

    public void ClearAttacking()
    {
        attacking = false;
    }
}