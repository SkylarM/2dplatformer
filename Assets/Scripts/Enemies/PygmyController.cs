﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PygmyController : MonoBehaviour
{
    private HealthController health;
    private CharacterMotorEnemies motor;
    private Animator anim;
    private int dir;
    private Vector2 velocity;
    private bool dead = false;

    public int damage = 1;
    public LayerMask layerMask;

    bool inZone;

    private void OnEnable()
    {
        TikiAttack.EnterTrigger += Charge;
        TikiAttack.ExitTrigger += EndCharge;
    }

    private void OnDisable()
    {
        TikiAttack.EnterTrigger -= Charge;
        TikiAttack.ExitTrigger += EndCharge;
    }

    void Awake()
    {
        inZone = false;
        dir = -5;
        anim = GetComponent<Animator>();
        motor = GetComponent<CharacterMotorEnemies>();
        health = GetComponent<HealthController>();
    }

    void Update()
    {
        if (!inZone && health.health != 0)
        {
            anim.SetTrigger("Walk");
            velocity = Vector3.one * dir;
            velocity.y = 0;
            motor.targetVelocity = velocity;
        }

        if (inZone && health.health != 0)
        {
            anim.SetTrigger("Run");
            velocity = PlayerController.instance.transform.position - transform.position;
            velocity.y = 0;
            motor.targetVelocity = velocity.normalized * 5;
        }

        if (health.health <= 0)
        {
            motor.targetVelocity = Vector2.zero;
            anim.SetTrigger("Dead");
            TikiAttack.EnterTrigger -= Charge;
            TikiAttack.ExitTrigger += EndCharge;
            if (!dead)
            {
                GetComponent<Collider2D>().enabled = !GetComponent<Collider2D>().enabled;
            }
            dead = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        dir = -dir;

        if (collision.gameObject.GetComponent<HealthController>() != null && collision.gameObject.GetComponent<HealthController>().health != 0 && !dead)
        {
            collision.gameObject.GetComponent<HealthController>().TakeDamage(damage);
        }
    }

    void Charge()
    {
        inZone = true;
    }
    void EndCharge()
    {
        inZone = false;
    }
}