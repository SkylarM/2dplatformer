﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathTriggers : MonoBehaviour {

    public GameObject deathScreen;
	public float damage = 5;

    void OnTriggerEnter2D(Collider2D c) {
		if (c.gameObject.CompareTag("Player")) {
			c.gameObject.transform.position = new Vector3(-10f, -3f, 0);
       		HitObject(c.gameObject);
		}
    }

    void HitObject(GameObject g) {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null) {
            health.TakeDamage(damage);
        }
    }
}

