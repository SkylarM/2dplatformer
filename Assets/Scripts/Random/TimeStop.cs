﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeStop : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.CompareTag("Player")) {
			StartCoroutine(DemoPause());
		}
	}

	IEnumerator DemoPause () {
		Time.timeScale = 0f;
		yield return new WaitForSeconds(4);
		Time.timeScale = 1f;

	}
}
