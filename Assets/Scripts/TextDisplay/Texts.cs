﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Texts : MonoBehaviour {

	//public UnityEvent e;

	public GameObject trigger;
	public GameObject bossSlider;

	void Start () {
		trigger.SetActive(true);
		bossSlider.SetActive(false);
	}

	void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.CompareTag("Player")) {
			//bossSlider.SetActive(true);
			TextController.ShowText("\tYou Shall Not pass.\n");
			TextController.ShowText("I will kill you now!");
			trigger.SetActive(false);
			//TextController.CallFunc(e.Invoke);
			//TextController.AddText("See you next summer!");
		}
	}
}
