﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

	public static TextController instance;

	public GameObject textBox;
	public Text uiText;
	public float typeDuration = 0.05f;
	
	Queue<IEnumerator> textQueue = new Queue<IEnumerator>();

	public delegate void TextCallBack();

	void Awake () {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else {
			Destroy(gameObject);
		}
	}

	void SetUp () {
		textBox.SetActive(true);
		uiText.text = "";
		//Time.timeScale = 0;
	}

	void Cleanup () {
		textBox.SetActive(false);
		uiText.text = "";
		//Time.timeScale = 1f;
	}

	IEnumerator Start () {
		Cleanup();
		while (enabled) {
			if (textQueue.Count > 0) {
				SetUp();
				while (textQueue.Count > 0) {
					yield return StartCoroutine(textQueue.Dequeue());
				}
				Cleanup();
			}
			yield return new WaitForEndOfFrame();
		}
	}

	public static void ShowText (string text) {
		instance.textQueue.Enqueue(instance.AddTextAndWait(text, true));
	}

	public static void AddText (string text) {
		instance.textQueue.Enqueue(instance.AddTextAndWait(text));
	}

	public static void CallFunc (TextCallBack func) {
		instance.textQueue.Enqueue(instance.CallFuncCoroutine(func));
	}

	IEnumerator CallFuncCoroutine (TextCallBack func) {
		func();
		yield return new WaitForEndOfFrame();
	}

	IEnumerator AddTextAndWait (string text, bool clear = false) {
		if (clear) uiText.text = "";
		yield return StartCoroutine(AddTextCoroutine(text));
		yield return StartCoroutine(WaitForSubmit());
	}
	
	IEnumerator AddTextCoroutine (string text) {
		for (int i = 0; i < text.Length; i++) {
			uiText.text += text[i];
			if (Input.GetButton("Submit")) yield return new WaitForEndOfFrame();
			else yield return new WaitForSecondsRealtime(typeDuration);
		}
	}

	IEnumerator WaitForSubmit () {
		while (!Input.GetButton("Submit")) yield return new WaitForEndOfFrame();
		while (Input.GetButton("Submit")) yield return new WaitForEndOfFrame();
	}
}
