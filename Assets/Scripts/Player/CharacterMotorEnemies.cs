﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotorEnemies : MonoBehaviour
{
    public Transform wallCheck;
    public float speed = 3;
    public float normalGravityScale = 1;

    public Rigidbody2D body;
    public Vector2 targetVelocity;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        body.gravityScale = normalGravityScale;
    }

    void FixedUpdate()
    {
        // orient character to target velocity
        if (targetVelocity.x > 0) transform.right = Vector3.left;
        else if (targetVelocity.x < 0) transform.right = Vector3.right;

        Vector2 velocityChange = targetVelocity - body.velocity;
        body.AddForce(velocityChange * body.mass / Time.fixedDeltaTime);
    }
}