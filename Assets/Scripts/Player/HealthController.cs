﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour {

	public delegate void OnHealthChanged (float previousHealth, float health);
	public event OnHealthChanged onHealthChanged = delegate {};

	public float maxHealth = 3;
	public float health;

	public float oldHealth;
	
	public bool isPlayer;

	bool canChange;
	public bool isRobot;
	public Slider roboSlider;

	void Awake () {
		health = maxHealth;
	}

	public void TakeDamage (float damage) {
		canChange = true;
		float oldHealth = health;
		
		if (isRobot) {
			roboSlider.value = health;
		}
		
		if (canChange) {
			health -= damage;
			health = Mathf.Clamp(health, 0, maxHealth);
			onHealthChanged(oldHealth, health);
		}
	}

	public void AddHealth (float h) {
		
			health += h;
			health = Mathf.Clamp(health, 0, maxHealth);
			onHealthChanged(oldHealth, health);
	}
}
