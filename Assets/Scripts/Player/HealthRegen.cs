﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRegen : MonoBehaviour {

	void OnTriggerStay2D (Collider2D c) {
		if (c.gameObject.CompareTag("Player")) {
			HealthController h = c.GetComponent<HealthController>();
			if (h.health < h.maxHealth) {
				h.AddHealth(1);
			}
		}
	}
}
