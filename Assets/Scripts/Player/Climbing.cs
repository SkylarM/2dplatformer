﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climbing : MonoBehaviour{

    private void OnTriggerEnter2D(Collider2D collision){

        PlayerController player = collision.gameObject.GetComponent<PlayerController>();

        if (player != null)
        {
            PlayerController.instance.canClimb = true;

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
          PlayerController player = collision.gameObject.GetComponent<PlayerController>();

        if (player != null){
            PlayerController.instance.canClimb = false;
            
        }
    }
}