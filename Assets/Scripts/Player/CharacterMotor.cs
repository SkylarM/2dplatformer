﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour {

	public Transform wallCheck;
	public float speed = 3;
	public float jumpHeight = 5;
	public LayerMask envLayers = -1;
	public float groundCheckDist = 0.1f;
	public float coyoteTime = 0.05f;
	public float jumpCancelHeight = 0.5f;
	public float normalGravityScale = 1;
	public float fallGravityScale = 1.5f;
	public float airMovementScale = 0.7f;
	public float wallSlideVelocity = -0.1f;
    public float wallJumpDelay = 0.25f;
	
	public Rigidbody2D body;
	public Vector2 targetVelocity;
	public bool shouldJump;
	public bool isWallSliding = false;
	public bool cancelJump;
    public bool onGround = false;
	private bool onWall = false;
	private bool canJump = false;
	private float lastGroundedTime = -1;
	
	void Start () {
		body = GetComponent<Rigidbody2D>();
		body.gravityScale = normalGravityScale;
		isWallSliding = false;
	}

    float wallJumpTime = -1;
	void FixedUpdate () {
		CheckGround();
		CheckWall();

		if (onWall && !onGround) {
			if (targetVelocity.x * transform.right.x > 0) targetVelocity.x = 0; // keeps you from sticking to the wall
			if (shouldJump && canJump) {
				canJump = false;
                wallJumpTime = Time.time;
                body.gravityScale = normalGravityScale;
				Vector2 v = Vector2.up * Mathf.Sqrt(2 * jumpHeight * Physics2D.gravity.magnitude * body.gravityScale); // wall jump
                v.x = -transform.right.x * targetVelocity.y;
                body.velocity = v;
			}
			else {
				targetVelocity.y = wallSlideVelocity; // wall slide
				isWallSliding = true;
			}
		}

        if (!onWall && !onGround)
        {
            PlayerController.canMove = false;
            if (Mathf.Abs(targetVelocity.x) < 0.5f)
            {
                targetVelocity.x = body.velocity.x;
            }
        }

		if (PlayerController.instance.sliding) {
			canJump = false;
		}

        if (shouldJump && canJump && (Time.time - lastGroundedTime) < coyoteTime) { // jumping slightly after you've fallen off a ledge
			canJump = false;
			body.gravityScale = normalGravityScale; // regular gravity going up
			targetVelocity.y = Mathf.Sqrt(2 * jumpHeight * Physics2D.gravity.magnitude * body.gravityScale); // initial velocity calculated from desired height
		}
		else if (!onGround && cancelJump && body.velocity.y > 0) {
			float jumpCancelVelocity = Mathf.Sqrt(2 * jumpCancelHeight * Physics2D.gravity.magnitude * body.gravityScale);
			if (targetVelocity.y > jumpCancelVelocity) targetVelocity.y = jumpCancelVelocity; // cancelling a jump should not be immediate
		}

		if (body.velocity.y < 0 && !onGround) body.gravityScale = fallGravityScale; // increased gravity when falling

		// orient character to target velocity
		if (targetVelocity.x > 0) transform.right = Vector3.right;
		else if (targetVelocity.x < 0) transform.right = Vector3.left;

        if (onGround || Time.time - wallJumpTime > wallJumpDelay)
        {
            Vector2 velocityChange = targetVelocity - body.velocity;
            if (!onGround) velocityChange.x *= airMovementScale;
            body.AddForce(velocityChange * body.mass / Time.fixedDeltaTime);
        }
	}

	void CheckWall () {
		RaycastHit2D hit = Physics2D.CircleCast(
			wallCheck.position,
			0.01f,
			wallCheck.right,
			0.01f,
			envLayers
		);

		onWall = hit.collider != null;
		if (onWall){
            canJump = true;
        }
	}

	void CheckGround () {
		RaycastHit2D hit = Physics2D.CircleCast(
			transform.position,
			0.2f,
			Vector2.down,
			groundCheckDist,
			envLayers
		);
		isWallSliding = false;

		//onGround = hit.collider != null;
		if (onGround) {
			body.gravityScale = normalGravityScale;
			lastGroundedTime = Time.time;
            canJump = true;
		}
	}

    void OnCollisionEnter2D(Collision2D c) {
        if (c.collider.gameObject.layer == LayerMask.NameToLayer("Environment"))
        {
            //PlayerController.instance.audio.PlayOneShot(landingSE);
        }
    }
}

