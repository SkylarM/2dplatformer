﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthControllerUI : MonoBehaviour {

	Slider slider;
	void Awake () {
		slider = GetComponent<Slider>();
	}

	bool needsRefresh = true;
	void Update () {
		if (needsRefresh && PlayerController.instance != null) {
			PlayerController.instance.GetComponent<HealthController>().onHealthChanged += OnHealthChanged;
			needsRefresh = false;
		}
		else if (!needsRefresh && PlayerController.instance == null) {
			needsRefresh = true;
		}
	}

	void OnHealthChanged (float previousHealth, float health) {
		slider.value = health;
	}
}
