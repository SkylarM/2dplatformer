# ABOUT

The protagonist is just a guy playing futuristic video games that put you in the game. He puts on a headset at the beginning of every level and enters the world.


# TEAM
- Skylar Masson: Main Character animations and script. UI design. Some level designing.
- Josh Gates: Enemy AI. 
- Sam Johnson: Level design. AI. Some Pixel Art.
- Mason Berry: Sounds. Effects. Menu


![alt text][MainCharacter.gif]


# STORY
Basic idea behind the story is that the protagonist is a guy in the future entering various virtual worlds through some sort of helmet or machine. This opens us up to practically infinite environments and enemies.
Each level could have it's own storyline, or the storyline could take a backseat and you just play through levels in various worlds.


# CONTROLS

Keyboard/Mouse:


Space - Jump


D - Move right


A - Move left


Left click - Attack 1


Right click - Attack 2



Xbox Controller:

A - Jump


Left Analog - Move left


Left Analog - Move right


X - Attack 1


B - Attack 2



# ENEMIES

- Medieval - Knights (Greatsword) and Archers(Longbow)


![alt text][Bandit1.png]	![alt text][Bandit2.gif]	![alt text][Militia.png]

	
- Pirate - Standard crewman [Look scrawny] (Cutlass), Buffer Crewman/Beards (Blunderbuss)


- Crypt - Skeleton (uses bones from the ground) , necromancer (Resurrects fallen skeletons and fires spells) , Zombie (swings his sword at the player)


![alt text][Skeleton.png]	![alt text][Zombie.png]


- Mount Fuji - Karate Disciples (martial arts, white dobak as their uniform), ninja (shuriken, black and red theme/uniform)

![alt text][NinjaEnemy.gif]


- Hell World - Hellhounds (on the ground, killed with pitchfork), Flaming Skulls (fly around, have a predictable attack pattern, killed by Watergun)


![alt text][EyeCreature.png]	![alt text][LavaDemon.png]	![alt text][Hellhound.gif]


# ITEMS


![alt text][SwordReference.png]


You are only be allowed to carry one weapon. You can choose which weapon you want to use from the previous levels at the beginning of each level, and you can hang on to it throughout the level or pick up weapons dropped by enemies along the way. There will be two thematic weapons on each level. Each weapon will have different properties with some weapons having special mechanics such as charge up attacks.


- Virtual tutorial - Cyber baton/cyber pistol.
*Baton - Generic melee weapon.


*Pistol - Shoots lazers.


- Medieval world - Greatsword/Longbow
*Greatsword - Heavy melee weapon; Relatively slow swing, high damage.


*Longbow - Can be charged to shoot further and make the arrow move faster.


- Pirate ship - Cutlass/Blunderbuss
*Cutlass - Fast melee weapon; Swings fast, does average damage.


*Blunderbuss - Close ranged gun; Can be charged to fire more projectiles.

- Crypt - Scythe/Fireball
*Scythe - Long melee weapon; Average stats, longer range.


*Fireball - fast flurry of ranged attacks.


- Mount Fuji - Martial Arts/Shuriken
*Martial Arts - Replaces base unarmed attack. Fast flurry of melee attacks with a charge up dragon punch attack.


*Shuriken - Fast, constant ranged attack.


- Hell world - Pitchfork/Watergun
*Pitchfork - Straight upgrade melee weapon.


*Watergun - Straight upgrade ranged weapon VS. hell enemies. Fairly useless otherwise.


# LEVELS
-Virtual tutorial


-Medieval world


-Pirate Ship


-Crypt


-Mount Fuji


-Hell world


# LEVEL PROPERTIES

- Medieval world - Start in a forest. Go through a town, accidentally push a lady down. Guards attack you.


- Crypt -Jumping on coffins. Have doors that need to be unlocked.



- Mount Fuji - Constantly climbing up. Have to jump on clouds. Crumbling platforms.



- Hell world - Level that is constantly sloping ever further downwards. Fire balls jump from lava pits beneath jumps. Lava falls from above that have breaks in them to jump through.


# TO-DO

- Make art for every Item, level, and enemy.


- Animate Player and enemy.


- Create Object motor.


- Create Player/AI controller.


- Enemy Sprites
##Medieval
* Bandit Warrior
* Town Guard
* Town Guard Archer


##Pirates
* Scrawny Crewman
* Healthy larger blunderbuss crewman


##Crypt
* Skeleton (melee)
* Necromancer


##Fuji
* Martial artist monk
* Ninja (Ranged)
* Bird


##Hell
* Hellhounds
* Burning Skulls
* Lava Demon
* Final Boss (Satan???)

#SOURCES
https://rvros.itch.io/animated-pixel-hero


https://stensven.itch.io/warrior-milita


https://finalgatestudios.itch.io/ninja-asset-pack


https://jesse-m.itch.io/skeleton-pack


https://lionheart963.itch.io/axe-bandit


https://stensven.itch.io/light-heavy-bandit


https://ansimuz.itch.io/hell-hound-sprite-animation


https://lionheart963.itch.io/wizard


https://finalgatestudios.itch.io/undead-sprite-pack


https://lionheart963.itch.io/the-summoner


https://stensven.itch.io/flame-demons-64-x64


https://lionheart963.itch.io/sorcerer-villain

#Image Links
[Skeleton.png]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Crypt/Skeleton.PNG "Crypt Skeleton"
[Zombie.png]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Crypt/Zombie.PNG "Crypt Zombie"
[EyeCreature.png]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/HellWorld/CreatureImage.PNG "Hell World Eye Creature"
[LavaDemon.png]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/HellWorld/DemonguyImage.PNG "Hell World Lava Demon"
[Hellhound.gif]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/HellWorld/Hellhound.gif "Hell World Hellhound"
[Bandit1.png]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Medieval/Bandit1.PNG "Medieval World Axe Bandit"
[Bandit2.gif]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Medieval/Bandit2.gif "Medieval World Sword Bandits"
[Militia.png]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Medieval/Milita.png "Medieval World Militia Guard"
[NinjaEnemy.gif]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/MountFuji/Ninja.gif "Mount Fuji Ninja Enemy"
[MainCharacter.gif]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/PlayerCharacter.gif "The Main Character"
[SwordReference.png]:https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/SwordReference.PNG "The main reference for our sword looks."
https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Crypt/Skeleton.PNG

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Crypt/Zombie.PNG

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/HellWorld/CreatureImage.PNG

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/HellWorld/DemonguyImage.PNG

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/HellWorld/Hellhound.gif

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Medieval/Bandit1.PNG

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Medieval/Bandit2.gif

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/Medieval/Milita.png

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/MountFuji/Ninja.gif

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/PlayerCharacter.gif

https://bitbucket.org/SkylarM/2dplatformer/raw/master/2DPlatformerReferences/SwordReference.PNG
